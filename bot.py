import telebot
import config
from telebot import types


bot = telebot.TeleBot(config.token)
password = "NOC2035"
TEAM_USER_LOGGING = 0
TEAM_USER_ACCEPTED = 1
CHANNEL_PR_NAME = '@nocshlack'
CHANNEL_NEWS_NAME = '@noc52_news'
pending_password_ids = []
auth_ids = []
press_ids = []
pending_press_ids = []


@bot.message_handler(content_types=["text"])
def send_text(message):
    if check_password(message):
        return
    if check_send_user_list(message):
        return
    if send_news(message):
        return
    if final_send_news(message):
        return
    if message.text == "Привет" or message.text == "привет":
        bot.send_message(message.from_user.id, "Привет, чем я могу тебе помочь?")
        n = open("usernames.txt", "a")
        n.write(str(message.from_user.username) + '\n')
        n.close()
    elif message.text == "/help":
        itembtn1 = types.KeyboardButton('Расскажи мне о НОЦ')
        itembtn2 = types.KeyboardButton('Отправить новость')
        itembtn3 = types.KeyboardButton('Связаться с пресс-службой')
        itembtn4 = types.KeyboardButton('Кто может стать партнером НОЦ?')
        itembtn5 = types.KeyboardButton('Что такое цифровая платформа?')
        itembtn6 = types.KeyboardButton('Как заявить о своем проекте?')
        itembtn7 = types.KeyboardButton('Как связаться с НОЦ?')
        markup = types.ReplyKeyboardMarkup()
        markup.row(itembtn1, itembtn2, itembtn3)
        markup.row(itembtn4)
        markup.row(itembtn5, itembtn6)
        markup.row(itembtn7)
        bot.send_message(message.chat.id, "Доступные функции:", reply_markup=markup)
    elif message.text == 'Расскажи мне о НОЦ':
        f1 = open("txt1.txt", "r", encoding="utf-8")
        bot.send_message(message.chat.id, f1.read())
        f1.close()
    elif message.text == 'Кто может стать партнером НОЦ?':
        f2 = open("partner.txt", "r", encoding="utf-8")
        bot.send_message(message.chat.id, f2.read())
        f2.close()
    elif message.text == 'Что такое цифровая платформа?':
        f3 = open("DP.txt", "r", encoding="utf-8")
        bot.send_message(message.chat.id, f3.read())
        f3.close()
    elif message.text == 'Как заявить о своем проекте?':
        f4 = open("project.txt", "r", encoding="utf-8")
        bot.send_message(message.chat.id, f4.read())
        f4.close()
    elif message.text == 'Как связаться с НОЦ?':
        f5 = open("connection.txt", "r", encoding="utf-8")
        bot.send_message(message.chat.id, f5.read())
        f5.close()
    elif message.text == 'Пока':
        bot.send_message(message.chat.id, "Пока")
    elif message.text == "Связаться с пресс-службой":
        bot.forward_message(756161584, message.chat.id, message.message_id)
        bot.forward_message(1321282695, message.chat.id, message.message_id)
        bot.send_message(message.chat.id, 'Напишите тему запроса, начиная со слова: "Тема"')
    elif message.text[0:4] == "Тема":
        bot.forward_message(756161584, message.chat.id, message.message_id)
        bot.forward_message(1321282695, message.chat.id, message.message_id)
        bot.send_message(message.chat.id, "Ваш запрос направлен в пресс-службу. Ожидайте")
    else:
        bot.send_message(message.from_user.id, "Я тебя не понимаю. Напиши /help.")


def check_password(message):
    if message.chat.id in pending_password_ids:
        if message.text == password:
            bot.send_message(message.chat.id, "Пароль верный")
            auth_ids.append(message.chat.id)
            num = open("numbers.txt", "a")
            num.write(str(message.chat.id) + '\t')
            num.write(str(message.from_user.first_name) + '\t')
            num.write(str(message.from_user.last_name) + '\n')
            num.close()
        else:
            bot.send_message(message.chat.id, "Пароль неверный")
        pending_password_ids.remove(message.chat.id)
        return True
    elif message.text == "Я модератор":
        bot.send_message(message.chat.id, "Введите пароль")
        pending_password_ids.append(message.chat.id)
        return True
    return False


def final_send_news(message):
    if message.chat.id in auth_ids:
        if message.chat.id in press_ids:
            if message.text != "":
                bot.send_message(CHANNEL_NEWS_NAME, message.text)
            else:
                bot.send_message(message.chat.id, "Уважаемый модератор, введите текст новости для публикации на канале")
            auth_ids.remove(message.chat.id)
            return True
        elif message.text == 'Опубликовать новость в новостном канале':
            bot.send_message(message.chat.id, "Уважаемый модератор, введите текст новости для публикации на канале")
            press_ids.append(message.chat.id)
            return True
        return False
    return False


def check_send_user_list(message):
    if message.text == "/list":
        if message.chat.id in auth_ids:
            bot.send_message(message.chat.id, "Список:"+str(auth_ids))
        else:
            bot.send_message(message.chat.id, "Вы не авторизованы")
        return True
    return False


def send_news(message):
    if message.chat.id in pending_press_ids:
        if message.text != "":
            bot.send_message(756161584, "Новость для публикации" + '\n' + '\n' + message.text)
            bot.send_message(1321282695, "Новость для публикации" + '\n' + '\n' + message.text)
            bot.send_message(message.chat.id, "Ваша новость направлена в пресс-службу. Ожидайте публикации в канале")
        else:
            bot.send_message(message.chat.id, "Введите текст новости")
        pending_press_ids.remove(message.chat.id)
        return True
    elif message.text[0:17] == 'Отправить новость':
        bot.send_message(message.chat.id, "Введите текст новости")
        pending_press_ids.append(message.chat.id)
        return True
    return False


if __name__ == '__main__':
    bot.infinity_polling()
