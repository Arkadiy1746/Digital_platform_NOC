FROM python:3

ADD config.py /
ADD txt.txt /
ADD bot.py /
ADD connection.txt /
ADD project.txt /
ADD numbers.txt /
ADD usernames.txt /
ADD partner.txt /
ADD DP.txt /
ADD txt1.txt /

RUN pip install pyTelegramBotApi

CMD [ "python", "./bot.py" ]